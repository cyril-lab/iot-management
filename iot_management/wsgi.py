"""
WSGI config for iot_management project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

production = os.environ.get('PRODUCTION')
if production and production == 'True':
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "iot_management.settings.production")
else:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "iot_management.settings.dev")


application = get_wsgi_application()
