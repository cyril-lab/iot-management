from .base import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'travis_ci_test',
        'USER': 'postgres',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    },
}

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@(ux_a_!igkp^vx8lqi%osm2v7uly3r8^trewq-pihz=2i3&^q'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

DEBUG = True