from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '@(ux_a_!igkp^vx8lqi%osm2v7uly3r8^trewq-pihz=2i3&^q'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'iot_management',
        'USER': 'iotmanagement',
        'PASSWORD': 'mw7mm1d48wunChTauTBJ',
        'HOST': 'localhost',
    }
}

try:
    from .local import *
except ImportError:
    pass
