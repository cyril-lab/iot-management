from django.urls import path
from . import views

urlpatterns = [
    path('', views.production_unit, name="production-unit"),
    path('creation-production-unit/',
         views.creation_production_unit, name="creation-production-unit"),
    path('delete-production-unit/<int:pk_unit>/',
         views.delete_production_unit, name="delete-production-unit"),
    path('production-unit-detail/<int:pk_unit>/',
         views.production_unit_detail, name="production-unit-detail"),
    path('creation-subunit/<int:pk_unit>/',
         views.creation_subunit, name="creation-subunit"),
    path('subunit/<int:pk_unit>/', views.subunit, name="subunit"),
    path('delete-subunit/<int:pk_subunit>/',
         views.delete_subunit, name="delete-subunit"),
    path('subunit-detail/<int:pk_subunit>/',
         views.subunit_detail, name="subunit-detail"),
    ]
