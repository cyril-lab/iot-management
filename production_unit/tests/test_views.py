from django.test import TestCase, Client, RequestFactory
from accounts.models import User
from production_unit.models.production_unit import ProductionUnit
from production_unit.models.subunit import Subunit


class ProductionUnitViewTest(TestCase):
    """this class test the production unit view"""
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        User.objects.create(username="jack", password='johnpassword', pk=1)
        self.user = User(username="jack", password='johnpassword', pk=1)
        self.client.force_login(self.user, backend=None)

        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='1',
                                      owner=User.objects.get(id=1))
        Subunit.objects.create(
            id=2,
            name="bassin 2",
            production_unit=ProductionUnit.objects.get(id=1))

    def test_production_unit_view(self):
        """this function test the creation production unit view"""
        response = self.client.get("/production-unit/")
        self.assertTemplateUsed(response,
                                'production_unit/production_unit.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_create_production_unit_view(self):
        """this function test the creation production unit view"""
        response = self.client.get(
            "/production-unit/creation-production-unit/")
        self.assertTemplateUsed(
            response,
            'production_unit/creation_production_unit.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_production_unit_detail_view(self):
        """this function test the production unit detail view"""
        response = self.client.get(
            "/production-unit/production-unit-detail/1/")
        self.assertTemplateUsed(
            response,
            'production_unit/production_unit_detail.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_delete_production_unit_view(self):
        """this function test to the delete production unit view"""
        response = self.client.get(
            "/production-unit/delete-production-unit/2/")
        self.assertRedirects(response, '/production-unit/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)

    def test_delete_subunit_view(self):
        """this function test to the delete subunit view"""
        response = self.client.get("/production-unit/delete-subunit/2/")
        self.assertRedirects(response, '/production-unit/subunit/1/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)

    def test_subunit_view(self):
        """this function test the creation subunit view"""
        response = self.client.get("/production-unit/subunit/1/")
        self.assertTemplateUsed(response, 'production_unit/subunit.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_create_subunit_view(self):
        """this function test the creation subunit view"""
        response = self.client.get("/production-unit/creation-subunit/1/")
        self.assertTemplateUsed(response,
                                'production_unit/creation_subunit.html')
        self.failUnlessEqual(response.status_code, 200)
