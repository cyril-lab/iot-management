from django.test import TestCase
from ..models.production_unit import ProductionUnit
from accounts.models import User
from ..models.subunit import Subunit


class ProductionUnitTest(TestCase):
    """this class test the production unit model"""
    def setUp(self):
        User.objects.create(username="jack", id=1)
        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='Aquaponie',
                                      owner=User.objects.get(id=1))

    def test_production_unit_saved(self):
        """this function tests if the user is registered """
        production_unit = ProductionUnit.objects.get(id=1)
        self.assertEqual(production_unit.name, 'aquaponie')

    def test_subunit_saved(self):
        """this function tests if the user is registered """
        Subunit.objects.create(
            id=1,
            name="bassin",
            production_unit=ProductionUnit.objects.get(id=1))
        subunit = Subunit.objects.get(id=1)
        self.assertEqual(subunit.name, 'bassin')
