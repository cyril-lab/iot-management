
from django.test import TestCase, Client, RequestFactory
from django.urls import reverse

from production_unit.models.production_unit import ProductionUnit
from accounts.models import User

from ...services import manage_production_unit
from ...services.manage_production_unit import ManageProductionUnit


class ManageProductionUnitTest(TestCase):
    """this class test the creation of a production unit"""

    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        User.objects.create(username="jack", password='johnpassword', pk=1)
        self.user = User(username="jack", password='johnpassword', pk=1)
        self.client.force_login(self.user, backend=None)

    def test_create_production_unit_and_get_form_data(self):
        """this function verify the creation of a production unit"""

        self.old_prod_unit = ProductionUnit.objects.count()
        self.form = {'name': 'aquaponie',
                     'type': '1',
                     'description': 'mon installation'}
        self.response = self.client.post(reverse('creation-production-unit'),
                                         {'name': self.form['name'],
                                          'type': self.form['type'],
                                          'description': self.form[
                                              'description']})

        self.new_prod_unit = ProductionUnit.objects.count()
        self.assertEqual(self.new_prod_unit, self.old_prod_unit + 1)

    def test_delete_production_unit(self):
        """this function verify the destruction of a production unit"""
        self.old_prod_unit = ProductionUnit.objects.count()
        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='1',
                                      owner=User.objects.get(id=1))
        self.new_prod_unit = ProductionUnit.objects.count()
        self.assertEqual(self.new_prod_unit, self.old_prod_unit+1)
        manage_production_unit.delete_production_unit(1)
        self.new_prod_unit = ProductionUnit.objects.count()
        self.assertEqual(self.new_prod_unit, self.old_prod_unit)

    def test_get_production_unit(self):
        """this function test to return the production unit"""
        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='1',
                                      owner=User.objects.get(id=1))
        ProductionUnit.objects.create(id=2,
                                      name="jardin",
                                      type='2',
                                      owner=User.objects.get(id=1))
        request = self.factory.get(reverse("production-unit"))
        request.user = self.user
        production_unit = ManageProductionUnit(request)
        result = production_unit.get_production_unit()
        self.assertEqual(len(result), 2)
