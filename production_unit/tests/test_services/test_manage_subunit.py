from django.test import TestCase, Client
from production_unit.models.production_unit import ProductionUnit
from production_unit.models.subunit import Subunit
from accounts.models import User
from ...services.manage_subunit import ManageSubunit


class ManageSubunitTest(TestCase):
    """this class test the creation of a subunit"""

    def setUp(self):
        self.client = Client()
        User.objects.create(username="jack", password='johnpassword', pk=1)
        self.user = User(username="jack", password='johnpassword', pk=1)
        self.client.force_login(self.user, backend=None)
        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='1',
                                      owner=User.objects.get(id=1))

    def test_create_subunit(self):
        """this function verify the creation of a subunit"""
        request = self.client.get("production-unit/creation-subunit/")
        request.user = self.user
        subunit = ManageSubunit()
        subunit.set_request(request)
        self.old_subunit = Subunit.objects.count()
        subunit.create_subunit(1)
        self.new_subunit = Subunit.objects.count()
        self.assertEqual(self.new_subunit, self.old_subunit + 1)

    def test_delete_subunit(self):
        """this function verify the destruction of a subunit"""
        self.old_subunit = Subunit.objects.count()
        Subunit.objects.create(
            id=1,
            name="bassin 1",
            production_unit=ProductionUnit.objects.get(id=1))
        self.new_subunit = ProductionUnit.objects.count()
        self.assertEqual(self.new_subunit, self.old_subunit + 1)
        subunit = ManageSubunit()
        subunit.set_pk_subunit(1)
        subunit.delete_subunit()
        self.new_subunit = Subunit.objects.count()
        self.assertEqual(self.new_subunit, self.old_subunit)

    def test_get_pk_unit_of_subunit(self):
        """this function verify the function get_pk_unit_of_subunit"""
        Subunit.objects.create(
            id=3, name="bassin 1",
            production_unit=ProductionUnit.objects.get(id=1))
        subunit = ManageSubunit()
        subunit.set_pk_subunit(3)
        pk_unit = subunit.get_pk_unit_of_subunit()
        self.assertEqual(pk_unit, 1)
