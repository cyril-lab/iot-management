from ..models.subunit import Subunit
from ..models.production_unit import ProductionUnit
from measure.models import Measure


def get_list_type_measure_in_subunit(pk_subunit):
    """Return a list of type measure
    This function returns a list of type of measure
    in a subunit
    """
    list_type_measure = []
    measure_query = Measure.objects.filter(subunit=pk_subunit)
    for one_measure in measure_query:
        if one_measure.type_measure not in list_type_measure:
            list_type_measure.append(one_measure.type_measure)
    return list_type_measure


class ManageSubunit:
    """this class allows to manage a subunit"""
    def __init__(self):
        self.request = ""
        self.name_form = ""
        self.name = ""
        self.description_form = ""
        self.subunit = []
        self.pk = ""
        self.pk_production_unit = ""
        self.name_production_unit = ""

    def set_request(self, request):
        self.request = request

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.name_form = form.cleaned_data['name']
        self.description_form = form.cleaned_data['description']

    def create_subunit(self, pk_unit):
        """this function create production subunit in database"""
        production_unit = ProductionUnit.objects.get(pk=pk_unit)
        subunit = Subunit(name=self.name_form,
                          description=self.description_form,
                          production_unit=production_unit)
        subunit.save()

    def set_pk_subunit(self, pk_subunit):
        self.pk = pk_subunit

    def get_subunit(self, pk_unit):
        """this function return the production subunit"""
        production_unit = ProductionUnit.objects.get(pk=pk_unit)
        subunit = Subunit.objects \
            .filter(production_unit=production_unit)
        for p in subunit:
            self.subunit.append(
                [p.pk, p.name, p.description])
        return self.subunit

    def get_pk_unit_of_subunit(self):
        """this function return a pk production unit"""
        queryset = Subunit.objects.get(pk=self.pk)
        production_unit = queryset.production_unit_id
        self.pk_production_unit = production_unit
        return production_unit

    def get_name_production_unit(self):
        """this function return a name production unit"""
        queryset = ProductionUnit.objects.get(
            pk=self.get_pk_unit_of_subunit())
        production_unit_name = queryset.name
        self.name_production_unit = production_unit_name
        return production_unit_name

    def delete_subunit(self):
        """this function delete a production subunit"""
        Subunit.objects \
            .filter(pk=self.pk) \
            .delete()

    def get_subunit_name(self):
        """this function return the name of the subunit"""
        subunit_name = Subunit.objects.get(pk=self.pk)
        self.name = subunit_name.name
        return subunit_name.name
