from ..models.production_unit import ProductionUnit
from accounts.models.user import User


def delete_production_unit(pk_unit):
    """this function delete a production unit"""
    ProductionUnit.objects \
        .filter(pk=pk_unit)\
        .delete()


class ManageProductionUnit:
    """this class allows to manage a production unit"""

    def __init__(self, request):
        self.request = request
        self.name = ""
        self.type = ""
        self.description = ""
        self.prod_unit = []

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.name = form.cleaned_data['name']
        self.type = form.cleaned_data['type']
        self.description = form.cleaned_data['description']

    def create_production_unit(self):
        """this function create production unit in database"""
        user = User.objects.get(pk=self.request.user.pk)
        production_unit = ProductionUnit(name=self.name,
                                         type=self.type,
                                         description=self.description,
                                         owner=user)
        production_unit.save()

    def get_production_unit(self):
        """this function return the production unit"""
        user = User.objects.get(pk=self.request.user.pk)
        prod_unit = ProductionUnit.objects \
            .filter(owner=user)
        for p in prod_unit:
            self.prod_unit.append(
                [p.pk, p.name, p.type, p.description])
        return self.prod_unit
