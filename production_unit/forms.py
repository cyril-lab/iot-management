from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit


class CreateProductionUnitForm(forms.Form):
    """it's the form to create a production unit """
    TYPE_PRODUCTION_UNIT_CHOICES = (
        ("1", "Aquaponie"),
        ("2", "Jardin"),
    )
    name = forms.CharField(label='Nom', max_length=100)
    type = forms.ChoiceField(label='Type ',
                             choices=TYPE_PRODUCTION_UNIT_CHOICES)
    description = forms.CharField(label='Description',
                                  max_length=400,
                                  required=False)
    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer une unité de production',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True


class CreateSubunitForm(forms.Form):
    """it's the form to create a subunit """
    name = forms.CharField(label='Nom', max_length=100)
    description = forms.CharField(label='Description',
                                  max_length=100,
                                  required=False)
    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer une sous-unité',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True
