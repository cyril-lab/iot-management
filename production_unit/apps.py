from django.apps import AppConfig


class ProductionUnitConfig(AppConfig):
    name = 'production_unit'
