from django.shortcuts import render, redirect, get_object_or_404
from .forms import CreateProductionUnitForm, CreateSubunitForm
from .models.production_unit import ProductionUnit
from .models.subunit import Subunit
from .services import manage_production_unit
from .services.manage_subunit import ManageSubunit
from .services.manage_production_unit import ManageProductionUnit


def creation_production_unit(request):
    """this function allows to register a production unit"""
    production_unit = ManageProductionUnit(request)
    if production_unit.request.method == 'POST':
        form = CreateProductionUnitForm(request.POST)
        if form.is_valid():
            production_unit.get_form_data(form)
            production_unit.create_production_unit()
            return redirect('production-unit')
        return render(request,
                      'production_unit/creation_production_unit.html',
                      {'form': form})
    else:
        form = CreateProductionUnitForm()
        return render(request,
                      'production_unit/creation_production_unit.html',
                      {'form': form})


def production_unit(request):
    """this function allows to display the production unit"""
    search = ManageProductionUnit(request)
    unit = search.get_production_unit()
    return render(request, 'production_unit/production_unit.html',
                  {'production_unit': unit})


def production_unit_detail(request, pk_unit):
    """this function allows to display the detail of a production unit"""
    unit_detail = get_object_or_404(ProductionUnit, pk=pk_unit)
    return render(request, 'production_unit/production_unit_detail.html',
                  {'unit': unit_detail})


def delete_production_unit(request, pk_unit):
    """this function delete production unit"""
    manage_production_unit.delete_production_unit(pk_unit)
    return redirect('production-unit')


def creation_subunit(request, pk_unit):
    """this function allows to create a subunit"""
    subunit = ManageSubunit()
    subunit.set_request(request)
    if subunit.request.method == 'POST':
        form = CreateSubunitForm(request.POST)
        if form.is_valid():
            subunit.get_form_data(form)
            subunit.create_subunit(pk_unit)
            return redirect('subunit', pk_unit=pk_unit)
        return render(request, 'production_unit/creation_subunit.html',
                      {'form': form, 'production_unit': pk_unit})
    else:
        form = CreateSubunitForm()
        return render(request, 'production_unit/creation_subunit.html',
                      {'form': form, 'production_unit': pk_unit})


def subunit(request, pk_unit):
    """this function allows to display the subunit"""
    search = ManageSubunit()
    sub = search.get_subunit(pk_unit)
    return render(request, 'production_unit/subunit.html',
                  {'subunit': sub, 'production_unit': pk_unit})


def delete_subunit(request, pk_subunit):
    """this function delete subunit"""
    subunit = ManageSubunit()

    subunit.set_pk_subunit(pk_subunit)
    pk_unit = subunit.get_pk_unit_of_subunit()
    subunit.delete_subunit()
    return redirect('subunit', pk_unit=pk_unit)


def subunit_detail(request, pk_subunit):
    """this function allows to display the detail of a subunit"""
    subunit_detail = get_object_or_404(Subunit, pk=pk_subunit)
    return render(request, 'production_unit/subunit_detail.html',
                  {'subunit': subunit_detail})
