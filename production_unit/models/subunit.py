from django.db import models
from .production_unit import ProductionUnit


class Subunit(models.Model):
    """this class is used to generate the subunit of production table"""
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    production_unit = models.ForeignKey(ProductionUnit,
                                        on_delete=models.CASCADE)

    def __str__(self):
        return self.pk
