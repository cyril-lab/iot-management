from django.db import models
from accounts.models.user import User


class ProductionUnit(models.Model):
    """this class is used to generate the production unit table"""
    name = models.CharField(max_length=200)
    type = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.pk
