### IOT MANAGEMENT PROGRAM

Author : VERNHES Cyril

#### Description :

This program allows you to record measurements. 
This program can be adapted to several types of installations. 
Can manage units and subunits.

First, you must create production unit and subunit.

After, you need to create units of measure and limits, if there are any (limits are not obligatory).

Finally, you can create the types of measures and you can create new measures in a subunit.

The dashboard displays the last measurement of each type of measurement.

#### Prerequisites :

To work, the program requires python 3.8.

#### Start program and virtual environment : 

Download the project with Git and open the command line and go to project path :

###### Create the directory of virtualenv files named venv :
`python3 -m venv venv`

###### Activate the virtual environment
`venv\Scripts\activate.bat`

###### Install the libraries
`pip install -r requirements.txt`

###### Settings
For development, it's the dev file which is used :

 `iot_management\settings\dev`
 
In production, it's the production file which is used.

 `iot_management\settings\production`
 
The file must be added in the same directory as the dev file with the informations on the production database, the secret key and the debug variable to False.
In production, you must add the environment variable : 

`PRODUCTION : True`

###### Migration of database :
After creating the database and the user :

`python manage.py makemigrations`

`python manage.py migrate`

###### Start test server :
`python3 manage.py runserver`

###### Stop virtual environment :
`deactivate`


###### Static files in production environment :
`python manage.py collectstatic`

###### To test the coverage rate:
`coverage run --source='.' manage.py test`

and

`coverage report` or `coverage html`

The file .coveragerc indicates the files not test.

###### Delete measures

`python3 manage.py measure --delete number_day`

For example : 

`python3 manage.py measure --delete 30`

Deletes the measurements present in the database for more than 30 days.

###### Count measures

`python3 manage.py measure --count number_day`

For example : 

`python3 manage.py measure --count 30`

Counts the number of measurements recorded since the last 30 days.
