from django.test import TestCase
from ..models.user import User


class UserTest(TestCase):
    """this class test the user model"""
    def setUp(self):
        User.objects.create(username="jack", id=1)

    def test_user_saved(self):
        """this function tests if the user is registered """
        user = User.objects.get(id=1)
        self.assertEqual(user.username, 'jack')
