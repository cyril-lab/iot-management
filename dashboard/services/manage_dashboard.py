from measure.models import Measure
from measure.services import manage_type_measure
from production_unit.services.manage_subunit import ManageSubunit
from measure.services.manage_measure \
    import get_list_subunit_in_measure_by_user
from production_unit.services.manage_subunit \
    import get_list_type_measure_in_subunit
from measure.services.manage_type_measure import ManageTypeMeasure


class ManageDashboard:
    """this class allows to manage dashboard"""

    def __init__(self, request):
        self.request = request
        self.measure = []

    def get_list_measure(self):

        list_subunit = get_list_subunit_in_measure_by_user(self.request)
        for each_subunit in list_subunit:
            list_type_measure = \
                get_list_type_measure_in_subunit(each_subunit.pk)
            for each_type_measure in list_type_measure:
                measure_query = Measure.objects\
                    .filter(subunit=each_subunit)\
                    .filter(type_measure=each_type_measure)\
                    .order_by('time')
                measure = measure_query.reverse()[:1]

                for pk in measure:
                    # get informations in measure
                    subunit = pk.subunit
                    type_measure = pk.type_measure
                    value = pk.value
                    time = pk.time

                    # search informations about subunit
                    # one_subunit = ManageSubunit(self.request)
                    one_subunit = ManageSubunit()
                    one_subunit.set_pk_subunit(pk.subunit.pk)
                    name_production_unit = \
                        one_subunit.get_name_production_unit()
                    mame_subunit = one_subunit.get_subunit_name()
                    pk_production_unit = \
                        one_subunit.get_pk_unit_of_subunit()

                    # search informations type measure
                    type_measure_unit = ManageTypeMeasure()
                    type_measure_unit.set_type_measure_pk(type_measure.pk)
                    unit_measure = \
                        type_measure_unit.get_type_measure_unit_name()
                    pk_type_measure = type_measure.pk

                    limit = manage_type_measure.verify_limit_type_measure(
                        value,
                        type_measure.pk)

                    self.measure.append([
                                        pk_production_unit,
                                        name_production_unit,
                                        subunit.pk,
                                        mame_subunit,
                                        pk_type_measure,
                                        type_measure,
                                        unit_measure,
                                        value,
                                        time,
                                        limit])
        return self.measure
