from django.shortcuts import render, redirect
from dashboard.services.manage_dashboard import ManageDashboard


def dashboard(request):
    """this function displays the home page"""
    if request.user.is_anonymous:
        return redirect('login')
    measure = ManageDashboard(request)
    measure_list = measure.get_list_measure()

    return render(request,
                  'dashboard/index.html',
                  {'measure_list': measure_list})
