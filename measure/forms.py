from crispy_forms.layout import Submit
from tempus_dominus.widgets import DateTimePicker
from measure.models import UnitMeasure
from measure.models import TypeMeasure
from django import forms
from crispy_forms.helper import FormHelper
from measure.models import LimitMeasure


class CreateUnitMeasure(forms.Form):
    unit_measure = forms.CharField(label='Unité de mesure', max_length=100)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer une unité de mesure',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True


class CreateLimitMeasure(forms.Form):
    limit_low = forms.DecimalField(label='Limite basse',
                                   decimal_places=2,
                                   max_digits=7,
                                   required=False)
    limit_high = forms.DecimalField(label='Limite haute',
                                    decimal_places=2,
                                    max_digits=7,
                                    required=False)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer une limite de mesure',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True


class CreateTypeMeasure(forms.Form):
    """it's the form to create a type of measure """

    type_unit = forms.CharField(label='Type de mesure', max_length=100)

    unit_measure = forms.ModelMultipleChoiceField(
        queryset=UnitMeasure.objects.all(),
        label='Unité de mesure')

    limit_measure = forms.ModelMultipleChoiceField(
        queryset=LimitMeasure.objects.all(),
        label='Limites mesure',
        required=False)

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer un type de mesures',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True


class CreateMeasure(forms.Form):
    """it's the form to create a type of measure """

    value = forms.DecimalField(label='Valeur mesure',
                               decimal_places=2,
                               max_digits=7)

    type_measure = forms.ModelMultipleChoiceField(
        queryset=TypeMeasure.objects.all(),
        label='Type de mesure')

    datetime_field = forms.DateTimeField(
        label='Date / heure',
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': True,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
            }
        ),
    )

    helper = FormHelper()
    helper.add_input(Submit('submit', 'Créer un type de mesures',
                            css_id="register-btn",
                            css_class='btn btn-primary btn-block'))
    helper.form_method = 'POST'
    helper.form_show_errors = True
