from django.db import models


class UnitMeasure(models.Model):
    """this class is used to generate the unit of measure table"""
    unit_measure = models.CharField(max_length=10)

    def __str__(self):
        return str(self.unit_measure)
