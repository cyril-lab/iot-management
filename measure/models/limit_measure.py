from django.db import models


class LimitMeasure(models.Model):
    """this class is used to generate the limit table"""
    limit_low = models.DecimalField(decimal_places=2,
                                    max_digits=7,
                                    null=True
                                    )
    limit_high = models.DecimalField(decimal_places=2,
                                     max_digits=7,
                                     null=True)

    def __str__(self):
        if self.limit_low and self.limit_high is None:
            return str(self.limit_low) + " - " + "No high limit"
        elif self.limit_low is None and self.limit_high:
            return "No low limit" + " - " + str(self.limit_high)
        else:
            return str(self.limit_low) + " - " + str(self.limit_high)
