from django.db import models
from measure.models.unit_measure import UnitMeasure
from measure.models.limit_measure import LimitMeasure


class TypeMeasure(models.Model):
    """this class is used to generate the type of measure table"""
    type_unit = models.CharField(max_length=200)
    unit_measure = models.ForeignKey(UnitMeasure,
                                     on_delete=models.CASCADE,
                                     null=True,
                                     blank=True)
    limit_measure = models.ForeignKey(LimitMeasure,
                                      on_delete=models.CASCADE,
                                      null=True,
                                      blank=True)

    def __str__(self):
        return str(self.type_unit)
