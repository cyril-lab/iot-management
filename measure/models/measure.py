from django.db import models
from django.utils.datetime_safe import datetime
from measure.models.type_measure import TypeMeasure
from production_unit.models.subunit import Subunit


class Measure(models.Model):
    """this class is used to generate the measure table"""
    time = models.DateTimeField(default=datetime.now)
    value = models.DecimalField(decimal_places=2, max_digits=7)
    type_measure = models.ForeignKey(TypeMeasure, on_delete=models.CASCADE)
    subunit = models.ForeignKey(Subunit, on_delete=models.CASCADE)

    def __str__(self):
        return self.pk
