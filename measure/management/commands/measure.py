from django.core.management.base import BaseCommand
from django.utils import timezone
from measure.models import Measure


class Command(BaseCommand):

    help = 'Delete measure'

    def add_arguments(self, parser):
        parser.add_argument(
                    '--delete',
                    action='store_true',
                    help='Delete measure',
                )
        parser.add_argument(
                    '--count',
                    action='store_true',
                    help='Count measure',
                )
        parser.add_argument('duration', type=int)

    def handle(self, *args, **options):
        if options['delete']:
            date = timezone.now() - \
                   timezone.timedelta(days=options['duration'])
            number_measure_delete = Measure.objects\
                .filter(time__lte=date).count()
            Measure.objects.filter(time__lte=date).delete()
            number_measure_after = Measure.objects\
                .filter(time__lte=date).count()
            if number_measure_after == 0:
                self.stdout.write(
                    f'{number_measure_delete} mesures '
                    f'suprimées avant : {date.date()}')

        elif options['count']:
            date = timezone.now() - \
                   timezone.timedelta(days=options['duration'])
            number_measure = Measure.objects\
                .filter(time__gte=date).count()

            self.stdout.write(
                f'{number_measure} mesures présentes '
                f'dans la base à partir de : {date.date()}')
