from django.test import TestCase, Client, RequestFactory
from django.utils import timezone
from ..models import Measure, TypeMeasure, UnitMeasure, LimitMeasure
from production_unit.models.production_unit import ProductionUnit
from accounts.models import User
from production_unit.models.subunit import Subunit


class MeasureViewTest(TestCase):
    """this class test the production unit view"""
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        User.objects.create(username="jack", password='johnpassword', pk=1)
        self.user = User(username="jack", password='johnpassword', pk=1)
        self.client.force_login(self.user, backend=None)

        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='1',
                                      owner=User.objects.get(id=1))
        Subunit.objects.create(
            id=1,
            name="bassin 1",
            production_unit=ProductionUnit.objects.get(id=1))

        LimitMeasure.objects.create(id=1,
                                    limit_low=10,
                                    limit_high=30)

        UnitMeasure.objects.create(id=1,
                                   unit_measure="C")

        TypeMeasure.objects.create(id=1,
                                   type_unit="Température",
                                   unit_measure=UnitMeasure.objects.get(id=1),
                                   limit_measure=LimitMeasure.objects.get(
                                       id=1))

        Measure.objects.create(id=2,
                               time=timezone.now(),
                               value=20,
                               type_measure=TypeMeasure.objects.get(id=1),
                               subunit=Subunit.objects.get(id=1))

    def test_creation_unit_measure_view(self):
        """this function test the creation unit measure view"""
        response = self.client.get("/measure/creation-unit-measure/")
        self.assertTemplateUsed(response,
                                'measure/creation_unit_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_unit_measure_view(self):
        """this function test the unit measure view"""
        response = self.client.get("/measure/unit-measure/")
        self.assertTemplateUsed(response,
                                'measure/unit_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_creation_type_measure_view(self):
        """this function test the creation of a type measure view"""
        response = self.client.get("/measure/creation-type-measure/")
        self.assertTemplateUsed(response,
                                'measure/creation_type_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_type_measure_view(self):
        """this function test a type of measure view"""
        response = self.client.get("/measure/type-measure/")
        self.assertTemplateUsed(response,
                                'measure/type_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_creation_limit_measure_view(self):
        """this function test the limit creation view"""
        response = self.client.get("/measure/creation-limit-measure/")
        self.assertTemplateUsed(response,
                                'measure/creation_limit_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_limit_measure_view(self):
        """this function test the limit of a measure view"""
        response = self.client.get("/measure/limit-measure/")
        self.assertTemplateUsed(response,
                                'measure/limit_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_creation_measure_view(self):
        """this function test the measure creation view"""
        response = self.client.get("/measure/creation-measure/1/")
        self.assertTemplateUsed(response,
                                'measure/creation_measure.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_delete_unit_measure_view(self):
        """this function test delete_unit_measure view"""
        response = self.client.get("/measure/delete-unit-measure/1/")
        self.assertRedirects(response, '/measure/unit-measure/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)

    def test_delete_type_measure_view(self):
        """this function test delete_type_measure view"""
        response = self.client.get("/measure/delete-type-measure/1/")
        self.assertRedirects(response, '/measure/type-measure/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)

    def test_delete_limit_measure_view(self):
        """this function test delete_limit_measure view"""
        response = self.client.get("/measure/delete-limit-measure/1/")
        self.assertRedirects(response, '/measure/limit-measure/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)

    def test_statistic_view(self):
        """this function test statistic view"""
        response = self.client.get("/measure/statistic/1/1/")
        self.assertTemplateUsed(response,
                                'measure/measure_statistic.html')
        self.failUnlessEqual(response.status_code, 200)

    def test_generate_view(self):
        """this function test generate_measure view"""
        self.old_measure = Measure.objects.count()
        response = self.client.get("/measure/generate-measure/1/")
        self.new_measure = Measure.objects.count()
        self.assertEqual(self.new_measure, self.old_measure + 1)
        self.assertRedirects(response, '/',
                             status_code=302,
                             target_status_code=200,
                             fetch_redirect_response=True)
