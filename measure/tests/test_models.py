from django.test import TestCase
from django.utils import timezone

from ..models import Measure, TypeMeasure, UnitMeasure, LimitMeasure
from production_unit.models.production_unit import ProductionUnit
from accounts.models import User
from production_unit.models.subunit import Subunit


class MeasureUnitTest(TestCase):
    """this class test the models of measure app"""
    def setUp(self):
        User.objects.create(username="jack", id=1)
        ProductionUnit.objects.create(id=1,
                                      name="aquaponie",
                                      type='Aquaponie',
                                      owner=User.objects.get(id=1))
        Subunit.objects.create(id=1,
                               name="bassin 1",
                               description='bassin poisson',
                               production_unit=ProductionUnit.objects.get(
                                   id=1))

        LimitMeasure.objects.create(id=1,
                                    limit_low=10,
                                    limit_high=30)

        UnitMeasure.objects.create(id=1,
                                   unit_measure="C")

        TypeMeasure.objects.create(id=1,
                                   type_unit="Température",
                                   unit_measure=UnitMeasure.objects.get(id=1),
                                   limit_measure=LimitMeasure.objects.get(
                                       id=1))

        Measure.objects.create(id=1,
                               time=timezone.now(),
                               value=20,
                               type_measure=TypeMeasure.objects.get(id=1),
                               subunit=Subunit.objects.get(id=1))

    def test_limit_saved(self):
        """this function tests if the limit is registered """
        limit = LimitMeasure.objects.get(id=1)
        self.assertEqual(limit.limit_low, 10)
        self.assertEqual(limit.limit_high, 30)

    def test_unit_measure_saved(self):
        """this function tests if the unit measure is registered """
        unit_measure = UnitMeasure.objects.get(id=1)
        self.assertEqual(unit_measure.unit_measure, 'C')

    def test_type_measure_saved(self):
        """this function tests if the type of measure is registered """
        type_measure = TypeMeasure.objects.get(id=1)
        self.assertEqual(type_measure.type_unit, 'Température')

    def test_measure_saved(self):
        """this function tests if the measure is registered """
        measure = Measure.objects.get(id=1)
        self.assertEqual(measure.value, 20)
        self.assertEqual(measure.type_measure.pk, 1)
        self.assertEqual(measure.subunit.pk, 1)
