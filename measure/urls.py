from django.urls import path
from . import views

urlpatterns = [

    path('creation-unit-measure/',
         views.creation_unit_measure, name="creation-unit-measure"),
    path('unit-measure/',
         views.unit_measure, name="unit-measure"),
    path('delete-unit-measure/<int:pk_unit_measure>/',
         views.delete_unit_measure, name="delete-unit-measure"),
    path('creation-type-measure/',
         views.creation_type_measure, name="creation-type-measure"),
    path('type-measure/',
         views.type_measure, name="type-measure"),
    path('delete-type-measure/<int:pk_type_measure>/',
         views.delete_type_measure, name="delete-type-measure"),
    path('creation-measure/<int:pk_subunit>/',
         views.creation_measure, name="creation-measure"),
    path('generate-measure/<int:duration>/',
         views.generate_measure, name="generate-measure"),
    path('creation-limit-measure/',
         views.creation_limit_measure, name="creation-limit-measure"),
    path('limit-measure/',
         views.limit_measure, name="limit-measure"),
    path('delete-limit-measure/<int:pk_limit_measure>/',
         views.delete_limit_measure, name="delete-limit-measure"),
    path('statistic/<int:pk_subunit>/<int:pk_type_measure>/',
         views.statistic, name="statistic"),

    ]
