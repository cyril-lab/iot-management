from measure.models import UnitMeasure


def get_unit_measure():
    """this function return the unit measure"""
    query_unit_measure = UnitMeasure.objects.all()
    unit_measure = []
    for unit in query_unit_measure:
        unit_measure.append((unit.pk, unit.unit_measure))
    return unit_measure


def delete_unit_measure(pk_unit_measure):
    """this function delete a unit measure"""
    UnitMeasure.objects \
        .filter(pk=pk_unit_measure) \
        .delete()


class ManageUnitMeasure:
    """this class allows to manage unit measure"""

    def __init__(self, request):
        self.request = request
        self.unit_measure = ""

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.unit_measure = form.cleaned_data['unit_measure']

    def create_unit_measure(self):
        """this function create a unit measure in database"""
        unit_measure = UnitMeasure(unit_measure=self.unit_measure)
        unit_measure.save()

    def delete_unit_measure(self):
        """this function delete a unit measure in database"""
        unit_measure = UnitMeasure(unit_measure=self.unit_measure)
        unit_measure.save()
