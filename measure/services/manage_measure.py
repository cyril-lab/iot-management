from random import uniform
from django.db.models import Avg
from django.db.models.functions import datetime
from django.utils import timezone
from measure.models import TypeMeasure
from measure.models import Measure
from production_unit.models.subunit import Subunit
from production_unit.services.manage_subunit import \
    get_list_type_measure_in_subunit
from measure.services.manage_type_measure import ManageTypeMeasure
from accounts.models import User


def get_data_detail_chart(pk_subunit, pk_type_measure, duration):
    """this function return detail of data type measure for chart"""
    date = timezone.now() - timezone.timedelta(days=duration)
    list_category = []
    list_value = []

    while duration:
        date = date + timezone.timedelta(days=1)

        value = get_measure_average_by_day(pk_subunit,
                                           pk_type_measure,
                                           date.year,
                                           date.month,
                                           date.day)
        if value is not None:
            list_value.append(value)
            list_category.append(str(date.date()))
        duration = duration-1

    return [list_category, list_value]


def get_list_subunit_in_measure_by_user(request):
    """Return a list of subunit in measure
    This function returns a list of subunit in
    all measures
    """
    list_subunit = []
    user = User.objects.get(pk=request.user.pk)
    measure_query = Measure.objects.filter(
        subunit__production_unit__owner=user)
    for one_measure in measure_query:
        if one_measure.subunit not in list_subunit:
            list_subunit.append(one_measure.subunit)
    return list_subunit


def get_measure_average_by_day(pk_subunit,
                               pk_type_measure,
                               year,
                               month=None,
                               day=None):
    """this function return a average corresponding to a date"""
    if month is None and day is None:
        measure = Measure.objects.filter(subunit=pk_subunit) \
            .filter(type_measure=pk_type_measure) \
            .filter(time__year=year) \
            .aggregate(Avg('value'))
    elif day is None and month is not None:
        measure = Measure.objects.filter(subunit=pk_subunit) \
            .filter(type_measure=pk_type_measure) \
            .filter(time__year=year, time__month=month) \
            .aggregate(Avg('value'))
    else:
        measure = Measure.objects.filter(subunit=pk_subunit) \
            .filter(type_measure=pk_type_measure) \
            .filter(time__year=year, time__month=month, time__day=day) \
            .aggregate(Avg('value'))

    average = measure['value__avg']
    if average is not None:
        average = round(float(measure['value__avg']), 2)
    return average


def generate_measure_random(request, duration):
    """this function return random measures"""
    duration_random = duration
    list_subunit = get_list_subunit_in_measure_by_user(request)
    for each_subunit in list_subunit:
        list_type_measure = get_list_type_measure_in_subunit(each_subunit)
        for each_type_measure in list_type_measure:

            type_measure = TypeMeasure.objects.get(pk=each_type_measure.pk)
            subunit_measure = Subunit.objects.get(pk=each_subunit.pk)
            date = datetime.timezone.now()
            duration = duration_random
            type_measure_limit = ManageTypeMeasure()
            type_measure_limit.set_type_measure_pk(each_type_measure.pk)
            value_limit = type_measure_limit.get_limit_type_measure()

            limit_low_t = value_limit[0]
            limit_high_t = value_limit[1]

            if (limit_low_t is not None) and (limit_high_t is None):
                limit_low = float(limit_low_t) * 0.5
                limit_high = float(limit_low_t) * 1.5

            elif (limit_low_t is None) and (limit_high_t is not None):
                limit_low = float(limit_high_t) * 0.5
                limit_high = float(limit_high_t) * 1.5

            elif (limit_low_t == 0) and (limit_high_t == 0):
                limit_low = 5
                limit_high = 30
            else:

                limit_low = float(limit_low_t) * 0.5
                limit_high = float(limit_high_t) * 1.5

            while duration:

                value = uniform(limit_low, limit_high)

                measure = Measure(time=date,
                                  value=value,
                                  type_measure=type_measure,
                                  subunit=subunit_measure)
                measure.save()
                date = date - datetime.timezone.timedelta(days=1)
                duration = duration - 1


class ManageMeasure:
    """this class allows to manage measure"""

    def __init__(self):
        self.request = ""
        self.unit = []
        self.datetime = ""
        self.subunit = ""
        self.type_measure = ""
        self.value = ""

    def set_request(self, request):
        self.request = request

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.datetime = form.cleaned_data['datetime_field']
        self.type_measure = form.cleaned_data['type_measure']
        self.value = form.cleaned_data['value']

    def create_measure(self, pk_subunit):
        """this function create a measure in database"""
        type_pk = ""
        for pk in self.type_measure:
            type_pk = pk.pk
        type_measure = TypeMeasure.objects.get(pk=type_pk)

        subunit_measure = Subunit.objects.get(pk=pk_subunit)

        measure = Measure(time=self.datetime,
                          value=self.value,
                          type_measure=type_measure,
                          subunit=subunit_measure)
        measure.save()
