from measure.models import LimitMeasure


def get_limit_measure():
    """this function return the limit measure"""
    query_limit_measure = LimitMeasure.objects.all()
    limit_measure = []
    for limit in query_limit_measure:
        limit_measure.append((limit.pk,
                              limit.limit_low,
                              limit.limit_high))
    return limit_measure


def delete_limit_measure(pk_limit_measure):
    """this function delete a limit measure"""
    LimitMeasure.objects \
        .filter(pk=pk_limit_measure) \
        .delete()


class ManageLimitMeasure:
    """this class allows to manage a limit"""
    def __init__(self, request):
        self.request = request
        self.limit_low = ""
        self.limit_high = ""

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.limit_low = form.cleaned_data['limit_low']
        self.limit_high = form.cleaned_data['limit_high']

    def create_limit_measure(self):
        """this function create a unit measure in database"""
        limit_measure = LimitMeasure(limit_low=self.limit_low,
                                     limit_high=self.limit_high)
        limit_measure.save()
