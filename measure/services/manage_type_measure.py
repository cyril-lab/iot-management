from measure.models import UnitMeasure
from measure.models import TypeMeasure
from measure.models import LimitMeasure


def verify_limit_type_measure(value, pk_type_measure):
    """this function compares a value with the defined limits"""
    number_type_measure = TypeMeasure.objects.filter(
        pk=pk_type_measure).exclude(limit_measure__isnull=True).count()
    if number_type_measure != 0:
        query_type_measure = TypeMeasure.objects.get(pk=pk_type_measure)
        query_limit_measure = LimitMeasure.objects.get(
            pk=query_type_measure.limit_measure.pk)

        limit_low = query_limit_measure.limit_low
        limit_high = query_limit_measure.limit_high
        # I check that the high and low limits exist
        if limit_low and limit_high is not None:
            if value < limit_low:
                return "low"
            elif value > limit_high:
                return "high"
            elif limit_high >= value >= limit_low:
                return "good"
        # only the high limit exist
        elif limit_low is None:
            if value > limit_high:
                return "high"
            elif limit_high >= value:
                return "good"
        # only the low limit exist
        elif limit_high is None:
            if value < limit_low:
                return "low"
            elif value >= limit_low:
                return "good"


def get_type_measure():
    """this function return the type measure"""
    query_type_measure = TypeMeasure.objects.all()
    type_measure = []
    for type in query_type_measure:
        type_measure.append((type.pk,
                             type.type_unit,
                             type.unit_measure,
                             type.limit_measure))
    return type_measure


def delete_type_measure(pk_type_measure):
    """this function delete a type measure"""
    TypeMeasure.objects \
        .filter(pk=pk_type_measure) \
        .delete()


class ManageTypeMeasure:
    """this class allows to manage type measure"""

    def __init__(self):
        self.request = ""
        self.type_unit_form = ""
        self.unit_measure_form = ""
        self.limit_measure_form = ""
        self.pk = ""
        self.name = ""
        self.unit_measure_pk = ""
        self.unit_measure_name = ""
        self.limit_pk = ""
        self.limit_low = 0
        self.limit_high = 0

    def set_request(self, request):
        self.request = request

    def get_form_data(self, form):
        """this function retrieves the data from the form"""
        self.type_unit_form = form.cleaned_data['type_unit']
        self.unit_measure_form = form.cleaned_data['unit_measure']
        self.limit_measure_form = form.cleaned_data['limit_measure']

    def create_type_measure(self):
        """this function create a unit measure in database"""
        unit_pk = ""
        for pk in self.unit_measure_form:
            unit_pk = pk.pk
        unit = UnitMeasure.objects.get(pk=unit_pk)

        limit_pk = ""
        # I test if the type of measurement has limits
        if self.limit_measure_form:
            for limit_value in self.limit_measure_form:
                limit_pk = limit_value.pk
            limit = LimitMeasure.objects.get(pk=limit_pk)

            type_measure = TypeMeasure(type_unit=self.type_unit_form,
                                       unit_measure=unit,
                                       limit_measure=limit)
        else:
            type_measure = TypeMeasure(type_unit=self.type_unit_form,
                                       unit_measure=unit)

        type_measure.save()

    def set_type_measure_pk(self, pk_type_measure):
        self.pk = pk_type_measure

    def get_type_measure_name(self):
        """this function return the name of a type measure"""
        type_measure = TypeMeasure.objects.get(pk=self.pk)
        self.name = type_measure.type_unit
        return type_measure.type_unit

    def get_type_measure_unit_name(self):
        """this function return the unit name of a type measure"""
        type_measure = TypeMeasure.objects.get(pk=self.pk)
        unit_measure = UnitMeasure.objects.get(pk=type_measure.unit_measure.pk)
        self.unit_measure_name = unit_measure
        return unit_measure.unit_measure

    def get_type_measure_unit_pk(self):
        """this function return the unit pk of a type measure"""
        type_measure = TypeMeasure.objects.get(pk=self.pk)
        self.unit_measure_pk = type_measure.unit_measure
        return type_measure.unit_measure

    def get_limit_type_measure(self):
        """this function return limits"""
        number_type_measure = TypeMeasure.objects.filter(
            pk=self.pk).exclude(limit_measure__isnull=True).count()
        if number_type_measure != 0:
            query_type_measure = TypeMeasure.objects.get(pk=self.pk)
            query_limit_measure = LimitMeasure.objects.get(
                    pk=query_type_measure.limit_measure.pk)
            self.limit_low = query_limit_measure.limit_low
            self.limit_high = query_limit_measure.limit_high

        return self.limit_low, self.limit_high

    def get_limit_pk(self):
        """this function return pk limit"""
        query_type_measure = TypeMeasure.objects.get(pk=self.pk)
        limit_pk_query = query_type_measure.limit_measure
        self.limit_pk = limit_pk_query
        return limit_pk_query
