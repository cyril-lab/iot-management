from django.shortcuts import render, redirect
from .forms import CreateUnitMeasure, CreateTypeMeasure, CreateMeasure, \
    CreateLimitMeasure
from .services import manage_unit_measure, manage_type_measure, \
    manage_limit_measure, manage_measure
from .services.manage_limit_measure import ManageLimitMeasure, \
    get_limit_measure
from .services.manage_measure import ManageMeasure, generate_measure_random
from .services.manage_type_measure import ManageTypeMeasure, get_type_measure
from .services.manage_unit_measure import ManageUnitMeasure, get_unit_measure

from production_unit.services.manage_subunit import ManageSubunit


def creation_unit_measure(request):
    """this function allows to create a unit"""
    unit_measure = ManageUnitMeasure(request)
    if unit_measure.request.method == 'POST':
        form = CreateUnitMeasure(request.POST)
        if form.is_valid():
            unit_measure.get_form_data(form)
            unit_measure.create_unit_measure()
            return redirect('unit-measure')
        return render(request,
                      'measure/creation_unit_measure.html',
                      {'form': form})
    else:

        form = CreateUnitMeasure()
        return render(request,
                      'measure/creation_unit_measure.html',
                      {'form': form})


def unit_measure(request):
    """this function allows to display the unit measure"""
    unit = get_unit_measure()
    return render(request, 'measure/unit_measure.html',
                  {'unit_measure': unit})


def delete_unit_measure(request, pk_unit_measure):
    """this function allows to delete a unit measure"""
    manage_unit_measure.delete_unit_measure(pk_unit_measure)
    return redirect('unit-measure')


def creation_type_measure(request):
    """this function allows to create a type of unit"""
    type_measure = ManageTypeMeasure()
    type_measure.set_request(request)
    if type_measure.request.method == 'POST':
        form = CreateTypeMeasure(request.POST)
        if form.is_valid():
            type_measure.get_form_data(form)
            type_measure.create_type_measure()
            return redirect('type-measure')
        return render(request,
                      'measure/creation_type_measure.html',
                      {'form': form})
    else:
        form = CreateTypeMeasure()
        return render(request,
                      'measure/creation_type_measure.html',
                      {'form': form})


def type_measure(request):
    """this function allows to display the type measure"""
    type = get_type_measure()
    return render(request, 'measure/type_measure.html',
                  {'type_measure': type})


def delete_type_measure(request, pk_type_measure):
    """this function allows to delete a type measure"""
    manage_type_measure.delete_type_measure(pk_type_measure)
    return redirect('type-measure')


def creation_measure(request, pk_subunit):
    """this function allows to create a measure"""
    measure = ManageMeasure()
    measure.set_request(request)
    subunit = ManageSubunit()
    subunit.set_pk_subunit(pk_subunit)
    pk_unit = subunit.get_pk_unit_of_subunit()

    if measure.request.method == 'POST':
        form = CreateMeasure(request.POST)
        if form.is_valid():
            measure.get_form_data(form)
            measure.create_measure(pk_subunit)

        return render(request,
                      'measure/creation_measure.html',
                      {'form': form,
                       'pk_unit': pk_unit,
                       'pk_subunit': pk_subunit})
    else:
        form = CreateMeasure()
        return render(request,
                      'measure/creation_measure.html',
                      {'form': form,
                       'pk_unit': pk_unit,
                       'pk_subunit': pk_subunit})


def creation_limit_measure(request):
    """this function allows to create a limit"""
    limit_measure = ManageLimitMeasure(request)
    if limit_measure.request.method == 'POST':
        form = CreateLimitMeasure(request.POST)
        if form.is_valid():
            limit_measure.get_form_data(form)
            limit_measure.create_limit_measure()
            return redirect('limit-measure')
        return render(request,
                      'measure/creation_limit_measure.html',
                      {'form': form})
    else:
        form = CreateLimitMeasure()
        return render(request,
                      'measure/creation_limit_measure.html',
                      {'form': form})


def limit_measure(request):
    """this function allows to display the type measure"""
    limit = get_limit_measure()
    return render(request, 'measure/limit_measure.html',
                  {'limit_measure': limit})


def delete_limit_measure(request, pk_limit_measure):
    """this function allows to delete a type measure"""
    manage_limit_measure.delete_limit_measure(pk_limit_measure)
    return redirect('limit-measure')


def statistic(request, pk_subunit, pk_type_measure):
    """this function return a list of data for chart"""
    data_chart = manage_measure.get_data_detail_chart(
        pk_subunit,
        pk_type_measure,
        30)

    subunit = ManageSubunit()
    subunit.set_pk_subunit(pk_subunit)
    subunit = [subunit.get_subunit_name()]

    type_measure = ManageTypeMeasure()
    type_measure.set_type_measure_pk(pk_type_measure)
    type = [type_measure.get_type_measure_name()]
    unit = [type_measure.get_type_measure_unit_name()]

    data_category = data_chart[0]
    data_value = data_chart[1]
    return render(request, 'measure/measure_statistic.html',
                  {'categories': data_category,
                   'values': data_value,
                   'subunit': subunit,
                   'type_measure': type,
                   'unit': unit})


def generate_measure(request, duration):
    """this function generate measure"""
    generate_measure_random(request, duration)
    return redirect('dashboard')
