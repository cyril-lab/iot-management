import time
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from accounts.models import User

firefox_options = webdriver.FirefoxOptions()
firefox_options.headless = True


class SeleniumTest(StaticLiveServerTestCase):
    """this class performs function tests"""
    def setUp(self):
        self.selenium = webdriver.Firefox(firefox_options=firefox_options)

    def tearDown(self):
        self.selenium.quit()

    def test_selenium(self):
        """this function tests website with Selenium"""
        sel = self.selenium
        url = self.live_server_url

        # Test login page
        sel.get(url)
        self.assertIn('Login', sel.title)
        time.sleep(2)

        # Test signup
        sel.get("{}/{}".format(url, "accounts/register"))
        assert "Créer un compte" in sel.title
        sel.find_element_by_id('id_name').send_keys('test_name')
        sel.find_element_by_id('id_surname').send_keys('test_surname')
        sel.find_element_by_id('id_email')\
            .send_keys('adresse_mail@hotmail.fr')
        sel.find_element_by_id('id_password1').send_keys('mdp')
        sel.find_element_by_id('id_password2').send_keys('mdp')
        sel.find_element_by_id("register-btn").click()
        WebDriverWait(sel, 15)
        user = User.objects.get(username='adresse_mail@hotmail.fr')
        self.assertEqual(user.username, 'adresse_mail@hotmail.fr')
        self.assertEqual(user.first_name, 'test_name')
        time.sleep(1)

        # Test login
        sel.get("{}/{}".format(self.live_server_url, "accounts/login"))
        self.assertIn('Login', sel.title)
        sel.find_element_by_name('username')\
            .send_keys('adresse_mail@hotmail.fr')
        sel.find_element_by_name('password').send_keys('mdp')
        sel.find_element_by_id("login-btn").click()
        self.assertIn('Dashboard', sel.title)
        time.sleep(1)

        # Test create unit measure
        sel.get("{}/{}".format(self.live_server_url,
                               "measure/creation-unit-measure/"))
        sel.find_element_by_id('id_unit_measure').send_keys('°C')
        sel.find_element_by_id("register-btn").click()
        time.sleep(1)

        # Test create limit
        sel.get("{}/{}".format(self.live_server_url,
                               "measure/creation-limit-measure/"))
        sel.find_element_by_id('id_limit_low').send_keys('10')
        sel.find_element_by_id('id_limit_high').send_keys('30')
        sel.find_element_by_id("register-btn").click()
        time.sleep(1)

        # Test type measure
        sel.get("{}/{}".format(self.live_server_url,
                               "measure/creation-type-measure/"))
        sel.find_element_by_id('id_type_unit').send_keys('température')
        sel.find_element_by_id('id_unit_measure').send_keys('°C')
        sel.find_element_by_id('id_limit_measure').send_keys('1')
        sel.find_element_by_id("register-btn").click()
        time.sleep(1)

        # Test create production unit
        sel.get("{}/{}".format(self.live_server_url,
                               "production-unit/creation-production-unit/"))
        sel.find_element_by_id('id_name').send_keys('aquaponie')
        sel.find_element_by_id('id_type').send_keys('1')
        sel.find_element_by_id('id_description').send_keys('mon aquaponie')
        sel.find_element_by_id("register-btn").click()
        time.sleep(1)

        # Test create subunit
        sel.find_element_by_class_name("btn-secondary").click()
        sel.find_element_by_class_name("btn-primary").click()
        sel.find_element_by_class_name("btn-primary").click()
        sel.find_element_by_id('id_name').send_keys('mon bassin')
        sel.find_element_by_id('id_description')\
            .send_keys('mon bassin à poisson')
        sel.find_element_by_class_name("btn-primary").click()
        time.sleep(1)

        # Test create measure
        sel.find_element_by_class_name("btn-secondary").click()
        sel.find_element_by_id('id_datetime_field')\
            .send_keys('2021-04-20 18:26:52')
        sel.find_element_by_id('id_type_measure').send_keys('température')
        sel.find_element_by_id('id_value').send_keys('20')
        sel.find_element_by_class_name("btn-primary").click()
        time.sleep(1)

        # Test dashboard
        sel.get(url)
        self.assertIn('Dashboard', sel.title)
        sel.find_element_by_class_name("text-success").click()
        self.assertIn('Mes statistiques', sel.title)
        time.sleep(1)

        # Test logout
        sel.find_element_by_id("logout-btn").click()
        time.sleep(1)
        self.assertIn('Login', sel.title)
